#ifndef Stepper_h
#define Stepper_h

class Stepper
{
public:
    Stepper(int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm);
    
    void brake();
    void standby();
    void stop();
    void set_pos(int pos);
    void step(int pos, int micro);
    void debug(int pos, int micro);

private:
    int stby;
    int apwm, ain2, ain1;
    int bin1, bin2, bpwm;

    int ain1_state, ain2_state, apwm_state, bin1_state, bin2_state, bpwm_state;
};

#endif