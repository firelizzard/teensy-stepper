#include "Stepper.h"
#include <Arduino.h>

const int sine_table[] = {0,13,25,37,50,62,74,86,98,109,120,131,142,152,162,171,180,189,197,205,212,219,225,231,236,240,244,247,250,252,254,255,255};

Stepper::Stepper(int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm)
{
    this->apwm = apwm;
    this->ain2 = ain2;
    this->ain1 = ain1;
    this->stby = standby;
    this->bin1 = bin1;
    this->bin2 = bin2;
    this->bpwm = bpwm;

    pinMode(apwm, OUTPUT);
    pinMode(ain2, OUTPUT);
    pinMode(ain1, OUTPUT);
    pinMode(standby, OUTPUT);
    pinMode(bin1, OUTPUT);
    pinMode(bin2, OUTPUT);
    pinMode(bpwm, OUTPUT);

    digitalWrite(standby, LOW);
    digitalWrite(apwm, HIGH);
    digitalWrite(bpwm, HIGH);
}

void Stepper::standby() {
    digitalWrite(stby, LOW);
}

void Stepper::brake() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, HIGH);
    digitalWrite(ain2, HIGH);
    digitalWrite(bin1, HIGH);
    digitalWrite(bin2, HIGH);
}

void Stepper::stop() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, LOW);
    digitalWrite(ain2, LOW);
    digitalWrite(bin1, LOW);
    digitalWrite(bin2, LOW);
}

void Stepper::set_pos(int pos) {
    // DUAL
    // -------
    // N | A B
    // 0 | < <
    // 1 | > <
    // 2 | > >
    // 3 | < >
    ain1_state = pos == 0 || pos == 1;
    bin1_state = pos == 1 || pos == 2;
    ain2_state = pos == 2 || pos == 3;
    bin2_state = pos == 3 || pos == 0;
}

void Stepper::step(int pos, int micro) {
    if (micro < 0)
        micro = 0;
    else if (micro > 32)
        micro = 32;

    // calculate the PWMs
    if (pos == 0 || pos == 2) {
        apwm_state = sine_table[micro];
        bpwm_state = sine_table[32-micro];
    } else {
        apwm_state = sine_table[32-micro];
        bpwm_state = sine_table[micro];
    }
    
    // set all the pins
    digitalWrite(ain1, ain1_state);
    digitalWrite(ain2, ain2_state);
    digitalWrite(bin1, bin1_state);
    digitalWrite(bin2, bin2_state);
    analogWrite(apwm, apwm_state);
    analogWrite(bpwm, bpwm_state);
    digitalWrite(stby, HIGH);
}

void Stepper::debug(int pos, int micro) {
    Serial.printf("A %3d {%d %d}\tB %3d {%d %d}\t[%d, %d]\r\n", apwm_state, ain1_state, ain2_state, bpwm_state, bin1_state, bin2_state, pos, micro);
}