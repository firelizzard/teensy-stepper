#ifndef Stepper_h
#define Stepper_h

#include <stdint.h>

class Stepper
{
public:
    // 32 steps
    static const int MicroStepDivisor = 7;
    static const int MicroStepResolution = 1 << MicroStepDivisor;
    static const int MicroStepWave[];

    Stepper(int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm);
    
    void brake();
    void standby();
    void stop();
    void set_pos(uint8_t pos);
    void step(uint8_t pos, uint8_t micro);
    void debug(uint8_t pos, uint8_t micro);

private:
    int stby;
    int apwm, ain2, ain1;
    int bin1, bin2, bpwm;

    int ain1_state, ain2_state, apwm_state, bin1_state, bin2_state, bpwm_state;
};

#endif