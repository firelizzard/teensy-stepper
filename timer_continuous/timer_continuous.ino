#include "Stepper.h"
#include <Wire.h>
#include <Adafruit_BusIO_Register.h>
#include <Adafruit_I2CDevice.h>

#define STOP 0
#define FWD 1
#define REV 2

volatile int dir = STOP;
volatile bool first_step = true;
volatile bool cur_stepping = false;
volatile bool cur_fwd;
volatile int cur_micro;
volatile int cur_pos = 0;

volatile bool debug = false;

int step_div = 1;
const int rpm = 90;

IntervalTimer stepTimer;
Stepper stepper(14, 15, 16, 17, 18, 19, 20);

FASTRUN void step()
{
    int pos;

    if (cur_stepping) {
    step:
        pos = cur_pos % 4;
        cur_micro += 1 << (Stepper::MicroStepDivisor - step_div);
        if (cur_micro >= Stepper::MicroStepResolution) {
            cur_stepping = false;
            if (!cur_fwd)
                stepper.set_pos(pos);
        }
        stepper.step(pos, cur_micro);
        debug = true;
        return;
    }

    if (!dir) {
        stepper.brake();
        return;
    }

    cur_stepping = true;
    cur_fwd = dir == FWD;
    cur_micro = 0;
    if (cur_fwd) {
        cur_pos = (cur_pos + 1) % 200;
        pos = cur_pos % 4;
        stepper.set_pos(pos);
    } else {
        cur_pos = (cur_pos ? cur_pos : 200) - 1;
        pos = cur_pos % 4;
    }

    if (first_step) {
        stepper.step(pos, cur_micro);
        debug = true;
        first_step = false;
    } else {
        goto step;
    }
}

void setup() {
    Serial.begin(115200);

    if (!stepTimer.begin(step, 60. * 1000000 / 200 / rpm / (1 << step_div))) {
        Serial.println("Failed to start timer");
    }
}

void loop() {
    if (0 && debug) {
        Serial.printf("%d, %d, %2d\t", cur_fwd, cur_pos, cur_micro);
        stepper.debug(cur_pos % 4, cur_micro);
        debug = false;
    }

    if (!Serial.available()) return;
    int b = Serial.read();
    switch (b) {
    case 0x1b:
        break;

    case 0x20:
        noInterrupts();
        dir = STOP;
        interrupts();
        return;

    default:
        Serial.println(b, HEX);
        return;
    }

    while (!Serial.available()) {}
    b = Serial.read();
    if (b != 0x5b) {
        Serial.println(b, HEX);
        return;
    }

    while (!Serial.available()) {}
    b = Serial.read();
    switch (b) {
    case 'A':
        noInterrupts();
        if (++step_div > Stepper::MicroStepDivisor) {
            step_div = Stepper::MicroStepDivisor;
        }
        interrupts();
        Serial.printf("F%d (%fus)\r\n", step_div, 60. * 1000000 / 200 / rpm / (1 << step_div));
        stepTimer.update(60. * 1000000 / 200 / rpm / (1 << step_div));
        break;
    case 'B':
        noInterrupts();
        if (--step_div < 0) {
            step_div = 0;
        }
        interrupts();
        Serial.printf("F%d (%fus)\r\n", step_div, 60. * 1000000 / 200 / rpm / (1 << step_div));
        stepTimer.update(60. * 1000000 / 200 / rpm / (1 << step_div));
        break;
    case 'C':
        noInterrupts();
        dir = FWD;
        interrupts();
        break;
    case 'D':
        noInterrupts();
        dir = REV;
        interrupts();
        break;
    default:
        Serial.printf("^[%c\r\n", b);
        break;
    }

}