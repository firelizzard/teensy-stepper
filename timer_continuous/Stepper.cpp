#include "Stepper.h"
#include <Arduino.h>

const int Stepper::MicroStepWave[] = {0,3,6,9,13,16,19,22,25,28,31,34,37,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,100,103,106,109,112,115,117,120,123,126,128,131,134,136,139,142,144,147,149,152,154,157,159,162,164,167,169,171,174,176,178,180,183,185,187,189,191,193,195,197,199,201,203,205,207,208,210,212,214,215,217,219,220,222,223,225,226,228,229,231,232,233,234,236,237,238,239,240,241,242,243,244,245,246,247,247,248,249,249,250,251,251,252,252,253,253,253,254,254,254,255,255,255,255,255,255} ;

Stepper::Stepper(int apwm, int ain2, int ain1, int stby, int bin1, int bin2, int bpwm)
{
    this->apwm = apwm;
    this->ain2 = ain2;
    this->ain1 = ain1;
    this->stby = stby;
    this->bin1 = bin1;
    this->bin2 = bin2;
    this->bpwm = bpwm;

    pinMode(apwm, OUTPUT);
    pinMode(ain2, OUTPUT);
    pinMode(ain1, OUTPUT);
    pinMode(stby, OUTPUT);
    pinMode(bin1, OUTPUT);
    pinMode(bin2, OUTPUT);
    pinMode(bpwm, OUTPUT);

    digitalWrite(stby, LOW);
    digitalWrite(apwm, HIGH);
    digitalWrite(bpwm, HIGH);
}

void Stepper::standby() {
    digitalWrite(stby, LOW);
}

void Stepper::brake() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, HIGH);
    digitalWrite(ain2, HIGH);
    digitalWrite(bin1, HIGH);
    digitalWrite(bin2, HIGH);
}

void Stepper::stop() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, LOW);
    digitalWrite(ain2, LOW);
    digitalWrite(bin1, LOW);
    digitalWrite(bin2, LOW);
}

void Stepper::set_pos(uint8_t pos) {
    // DUAL
    // -------
    // N | A B
    // 0 | < <
    // 1 | > <
    // 2 | > >
    // 3 | < >
    ain1_state = pos == 0 || pos == 1;
    bin1_state = pos == 1 || pos == 2;
    ain2_state = pos == 2 || pos == 3;
    bin2_state = pos == 3 || pos == 0;
}

void Stepper::step(uint8_t pos, uint8_t micro) {
    if (micro < 0)
        micro = 0;
    else if (micro > MicroStepResolution)
        micro = MicroStepResolution;

    // calculate the PWMs
    if (pos == 0 || pos == 2) {
        apwm_state = MicroStepWave[micro];
        bpwm_state = MicroStepWave[MicroStepResolution-micro];
    } else {
        apwm_state = MicroStepWave[MicroStepResolution-micro];
        bpwm_state = MicroStepWave[micro];
    }
    
    // set all the pins
    digitalWrite(ain1, ain1_state);
    digitalWrite(ain2, ain2_state);
    digitalWrite(bin1, bin1_state);
    digitalWrite(bin2, bin2_state);
    analogWrite (apwm, apwm_state);
    analogWrite (bpwm, bpwm_state);
    digitalWrite(stby, HIGH);
}

void Stepper::debug(uint8_t pos, uint8_t micro) {
    Serial.printf("A %3d {%d %d}\tB %3d {%d %d}\t[%d, %d]\r\n", apwm_state, ain1_state, ain2_state, bpwm_state, bin1_state, bin2_state, pos, micro);
}