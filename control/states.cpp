#include "main.h"
#include "Screen.h"

extern Screen screen;
bool isGuiding();
void startGuiding();
void stopGuiding();

state_t sharedStateNop(void * data, int turn, int press);
state_t sharedStateMenu(void * data, int turn, int press);
state_t stateManualPosition(void * data, int turn, int press);
template<typename T> state_t sharedMutateInMenu(void * data, int turn, int press);

extern stringer_t string_targetPos;
extern struct mutate_args<int> mutate_bladeKerf, mutate_margin, mutate_rpm, mutate_posDelta, mutate_targetPos, mutate_cutWidth, mutate_guidedPos;

typedef struct menu * (*menu_getter_t)();

#define menu_getter (void *)(menu_getter_t)[]
#define state_fn [](void * data, int turn, int press)

struct state_table states[NUM_STATES] = {
    {"> MechanoJig <", sharedStateMenu,         menu_getter { return &mainMenu; }},
    {"   Settings   ", sharedStateMenu,         menu_getter { return &settingsMenu; }},
    {"    Manual    ", sharedStateMenu,         menu_getter { return &manualMenu; }},
    {"    Guided    ", sharedStateMenu,         menu_getter { return isGuiding() ? &guidedOnMenu : &guidedOffMenu; }},

    {"   Settings   ", sharedMutateInMenu<int>, &mutate_bladeKerf},
    {"   Settings   ", sharedMutateInMenu<int>, &mutate_margin},
    {"   Settings   ", sharedMutateInMenu<int>, &mutate_rpm},

    {"    Manual    ", sharedMutateInMenu<int>, &mutate_posDelta},
    {"    Manual    ", stateManualPosition,     NULL},

    {"    Guided    ", sharedMutateInMenu<int>, &mutate_cutWidth},
    {"              ", state_fn { startGuiding(); return StateGuided; }, NULL},
    {"              ", state_fn { stopGuiding(); return StateGuided; }, NULL},
    {"    Guided    ", sharedMutateInMenu<int>, &mutate_guidedPos},
};

bool wantsEncoderUpdates(state_t state) {
    return state == StateManual
        || state == StateManualDelta
        // || state == StateManualPosition
        || state == StateGuided
        || state == StateGuidedMove;
}

void menu_item_print(int row, const char * display, stringer_t displayf, bool highlight_text, bool highlight_value) {
    char s[15];
    int l = snprintf(s, 15, "%s", display);
    if (l > 14) l = 14;
    for (int i = l; i < 14; i++)
        s[i] = ' ';
    s[14] = 0;

    if (!displayf) {
        screen.print(0, row, s, 14, highlight_text);
        return;
    }

    int k = displayf(NULL, 0);
    displayf(&s[14-k], k+1);
    if (k + l >= 14) {
        s[12-k] = '\x82';
        s[13-k] = ' ';
    }

    if (highlight_text == highlight_value) {
        screen.print(0, row, s, 14, highlight_value);
    } else {
        l = 14-k;
        screen.print(0, row, s, l, highlight_text);
        screen.print(l*6, row, s+l, k, highlight_value);
    }
}

void menu_print(struct menu * menu, bool highlight) {
    for (int i = 0; i < menu->count; i++) {
        auto item = &menu->items[i];
        bool h = highlight && menu->selected == i;
        menu_item_print(i + 1, item->display, item->displayf, h, h);
    }
}

void menu_select(struct menu * menu, int turn) {
    if (turn > 0)
        menu->selected = (menu->selected + 1) % menu->count;
    else if (turn < 0)
        menu->selected = menu->selected ? menu->selected - 1 : menu->count - 1;
}

state_t menu_get(struct menu * menu) {
    return menu->items[menu->selected].state;
}
state_t sharedStateNop(void * data, int turn, int press) {
    if (press) {
        return ReturnState;
    }
    return SameState;
}

state_t sharedStateMenu(void * data, int turn, int press) {
    auto menu = ((menu_getter_t)data)();

    if (press)
        return menu_get(menu);

    menu_select(menu, turn);
    menu_print(menu, true);
    return SameState;
}

template<typename T>
state_t sharedMutateInMenu(void * data, int turn, int press) {
    auto args = (struct mutate_args<T> *)data;
    if (press) {
        if (args->on_press)
            args->on_press();
        return ReturnState;
    }

    if (turn) args->mutate(turn);

    menu_print(args->menu, false);

    auto item = &args->menu->items[args->menu->selected];
    if (!item->displayf) return SameState;

    char s[15];
    int l = item->displayf(s, 15);
    if (l > 14) l = 14;
    screen.print((14-l)*6, args->menu->selected+1, s, l, true);

    return SameState;
}

state_t stateManualPosition(void * _, int turn, int press) {
    if (press) return ReturnState;

    if (turn) mutate_targetPos.mutate(turn);

    menu_print(&manualMenu, false);

    auto item = &manualMenu.items[manualMenu.selected];
    menu_item_print(manualMenu.selected+1, item->display, string_targetPos, false, true);
    return SameState;
}
