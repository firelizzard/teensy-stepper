#include "Knob.h"
#include "Screen.h"
#include "Queue.h"
#include "Stepper.h"

#include "main.h"

const int RevPerInch = 18;
const int StepsPerRev = 200;

const int KnobA = 22, KnobB = 23;

IntervalTimer stepTimer;
ScreenControlSPI sctrl(&SPI, 35, 34, 33);
Screen screen(&sctrl);
Knob knob(21, KnobA, KnobB);
TurnDecoder turnd(&knob);
Stepper stepper(14, 15, 16, 17, 18, 19, 20);

#define floor(v, lb) { \
        if (v < lb) v = lb; \
    }

#define range(v, lb, ub) { \
        if (v < lb) v = lb; \
        else if (v > ub) v = ub; \
    }

#undef round
#define round(v, n) { \
        int r = v % n; \
        if (r) { \
            if (r < n/2) v -= r; \
            else v += n-r; \
        } \
    }

static constexpr int posDeltaConv[] = { 1, StepsPerRev/20, StepsPerRev/4, StepsPerRev, StepsPerRev*2 };

struct Settings {
    static constexpr unsigned int Magic = 0xDEADBEEF;

    unsigned int magic;
    int bladeKerf, margin, rpm;

    float interval() { return 60. * 1000000 / StepsPerRev / (rpm <= 0 ? 1 : rpm); }

    void dBladeKerf(int d) { bladeKerf += d; floor(bladeKerf, 0); }
    void dMargin(int d) { margin += d; }
    void dRPM(int d) { rpm += d * 10; floor(rpm, 10); round(rpm, 10); }

    Settings() {
        eeprom_read_block((void *)this, 0, sizeof(struct Settings));
        if (magic == Magic) return;

        magic = Magic;
        bladeKerf = 0;
        margin = 0;
        rpm = 20;
    }

    void write() {
        eeprom_write_block((void *)this, 0, sizeof(struct Settings));
    }

    void updateRPM() {
        stepTimer.update(interval());
        write();
    }
} settings;

struct Current {
    static const int maxPosDelta = LEN(posDeltaConv) - 1;

    bool guidedActive;
    int posDelta, cutWidth;
    int guidedPos, guidedStartPos, cutWidthSteps, passesPerCut;
    volatile int targetPos, actualPos;

    int convPosDelta() { return posDeltaConv[posDelta]; }

    void dPosDelta(int d) { posDelta += d; range(posDelta, 0, maxPosDelta); }
    void dTargetPos(int d) { targetPos += d * convPosDelta(); }
    void dCutWidth(int d) { cutWidth += d; floor(cutWidth, 1); }

    void dGuidedPos(int d) {
        guidedPos += d;
        floor(guidedPos, 0);

        int pos = guidedStartPos;
        if (cutWidth == 1)
            pos += guidedPos * settings.bladeKerf * 2;
        else {
            int slotNum = guidedPos / passesPerCut;
            int passNum = guidedPos % passesPerCut;

            pos += slotNum * cutWidthSteps * 2;

            if (passNum == passesPerCut - 1)
                pos += cutWidthSteps - settings.bladeKerf;
            else
                pos += passNum * settings.bladeKerf;
        }

        targetPos = pos;
    }

    Current() {
        posDelta = 2;
        targetPos = 0;
        actualPos = 0;
        cutWidth = 1;
        guidedActive = false;
    }

    bool step() {
        if (actualPos == targetPos) {
            stepper.brake();
            return false;
        }

        if (actualPos < targetPos)
            actualPos++;
        else
            actualPos--;

        int p = actualPos;
        while (p < 0) p += StepsPerRev;
        stepper.step(p % 4);
        return true;
    }

    void startGuiding() {
        guidedActive = true;
        guidedPos = 0;
        guidedStartPos = targetPos;

        if (cutWidth == 1) {
            cutWidthSteps = settings.bladeKerf;
            passesPerCut = 1;

        } else {
            cutWidthSteps = RevPerInch * StepsPerRev * cutWidth / 16;
            passesPerCut = cutWidthSteps / settings.bladeKerf;
            if (cutWidthSteps % settings.bladeKerf) passesPerCut++;
        }

        Serial.printf("Guiding with %d steps/cut, %d passes/cut\n", cutWidthSteps, passesPerCut);
    }

    void stopGuiding() {
        guidedActive = false;
    }
} current;

bool isGuiding() { return current.guidedActive; }
void startGuiding() { current.startGuiding(); }
void stopGuiding() { current.stopGuiding(); }

#define fmt_menu(v, fmt) [](char * buf, int n) { return snprintf(buf, n, fmt, v); }

constexpr char pos_int_fmt[] = "%d";
constexpr char int_fmt[] = "%+d";

stringer_t string_bladeKerf  = fmt_menu(settings.bladeKerf, pos_int_fmt);
stringer_t string_margin     = fmt_menu(settings.margin, int_fmt);
stringer_t string_rpm        = fmt_menu(settings.rpm, pos_int_fmt);
stringer_t string_posDelta   = fmt_menu(current.convPosDelta(), pos_int_fmt);
stringer_t string_actualPos  = fmt_menu(current.actualPos, pos_int_fmt);
stringer_t string_targetPos  = fmt_menu(current.targetPos, pos_int_fmt);
stringer_t string_actualPosG = fmt_menu(current.actualPos - current.guidedStartPos, pos_int_fmt);
stringer_t string_targetPosG = fmt_menu(current.targetPos - current.guidedStartPos, pos_int_fmt);

stringer_t string_cutWidth = [](char * buf, int n) {
    if (current.cutWidth == 1)
        return snprintf(buf, n, "kerf");
    switch (__builtin_ctz(current.cutWidth)) {
        case 0: return snprintf(buf, n, "%d/16\"", current.cutWidth);
        case 1: return snprintf(buf, n, "%d/ 8\"", current.cutWidth >> 1);
        case 2: return snprintf(buf, n, "%d/ 4\"", current.cutWidth >> 2);
        case 3: return snprintf(buf, n, "%d/ 2\"", current.cutWidth >> 3); default: // fallthrough
        case 4: return snprintf(buf, n, "%d   \"", current.cutWidth >> 4);
    }
};

stringer_t string_guidedPos = [](char * buf, int n) {
    if (current.cutWidth == 1)
        return snprintf(buf, n, "#%d", current.guidedPos + 1);

    int slotNum = current.guidedPos / current.passesPerCut;
    int passNum = current.guidedPos % current.passesPerCut;
    return snprintf(buf, n, "#%d, %d/%d", slotNum+1, passNum+1, current.passesPerCut);
};

struct mutate_args<int> mutate_bladeKerf = {&settingsMenu,  [] { settings.write(); },     [](int turn) { settings.dBladeKerf(turn); }};
struct mutate_args<int> mutate_margin    = {&settingsMenu,  [] { settings.write(); },     [](int turn) { settings.dMargin(turn);    }};
struct mutate_args<int> mutate_rpm       = {&settingsMenu,  [] { settings.updateRPM(); }, [](int turn) { settings.dRPM(turn);       }};
struct mutate_args<int> mutate_posDelta  = {&manualMenu,    NULL,                         [](int turn) { current.dPosDelta(turn);   }};
struct mutate_args<int> mutate_targetPos = {&manualMenu,    NULL,                         [](int turn) { current.dTargetPos(turn);  }};
struct mutate_args<int> mutate_cutWidth  = {&guidedOffMenu, NULL,                         [](int turn) { current.dCutWidth(turn);   }};
struct mutate_args<int> mutate_guidedPos = {&guidedOnMenu,  NULL,                         [](int turn) { current.dGuidedPos(turn);  }};

bool wantsEncoderUpdates(state_t state);

typedef enum {
    kLifeEvent,
    kSwitchEvent,
    kEncoderEvent,
    kStepperEvent,

    kDebugEvent
} event_source_t;

typedef struct {
    event_source_t source;
    int value;
} event_t;

Queue<event_t, 256> events;

#define push_event(...) do { event_t e = { __VA_ARGS__ }; events.push(e); } while(0);

void buttonISR() {
    push_event(kSwitchEvent, 0);
}

void encoderISR() {
    int turn = turnd.read();
    if (!turn) return;

    push_event(kEncoderEvent, turn);
}

void step() {
    static int last_event = 0, last_pos = current.actualPos;

    bool moved = current.step();

    // send an event if A) we moved and B) we reached the target or it's been 250ms since the last event
    int now = millis();
    if (last_pos != current.actualPos && (!moved || now - last_event > 250)) {
        last_event = now;
        last_pos = current.actualPos;
        push_event(kStepperEvent, current.actualPos);
    }
}

int main() {
    static auto state = StateMain;

    SPI.setSCK(13);
    SPI.setMOSI(11);
    SPI.begin();

    Serial.begin(115200);
    screen.begin();

    if (!stepTimer.begin(step, settings.interval()))
        Serial.println("Failed to start timer");

#if HAS_DEBUGGER
    debug.begin();
#endif

    knob.onSwitch(buttonISR);
    knob.onEncoder(encoderISR);

    push_event(kLifeEvent, 0);

    for (;;) {
        event_t e = {};
        if (!events.pop(&e)) {
            delay(1);
            continue;
        }

        if (Serial) {
            if (e.source == kDebugEvent)
                Serial.printf("Debug -> %d\r\n", e.value);
            else
                Serial.printf("Event -> %d, %+d\r\n", e.source, e.value);
        }

        int turn = 0, press = 0;
        switch (e.source) {
            case kLifeEvent:                    break;
            case kSwitchEvent:  press = 1;      break;
            case kEncoderEvent: turn = e.value; break;
            default: continue;

            case kStepperEvent:
                // only update if we're on a screen that shows the stepper position
                if (!wantsEncoderUpdates(state)) continue;
                break;
        }

        if (e.source == kEncoderEvent && !turn) continue;

        if (state < 0 || state >= NUM_STATES) {
            Serial.printf("Bad state: %d\r\n", state);
            state = StateMain;
        }

        auto * s = &states[state];

        screen.clear();
        screen.print(0, 0, s->header, -1, true);

        state_t next = s->fn(s->data, turn, press);
        if (next == SameState) continue;

        if (next == ReturnState)
            state = menuReturn[state];
        else
            state = next;

        Serial.printf("State -> %d\r\n", state);
        push_event(kLifeEvent, 0);
    }

    return 0;
}
