#ifndef main_h
#define main_h

#include <Arduino.h>
// #include <TeensyDebug.h>

#ifdef DEBUGRUN
#define HAS_DEBUGGER 1
#else
#define HAS_DEBUGGER 0
#define DEBUGRUN
#endif

#define LEN(v) (sizeof(v)/sizeof(v[0]))

extern struct state_table states[];
extern struct menu mainMenu, settingsMenu, manualMenu, guidedOffMenu, guidedOnMenu;

typedef enum {
    ReturnState = -2,
    SameState = -1,

    StateMain = 0,
    StateSettings,
    StateManual,
    StateGuided,

    StateSettingsBladeKerf,
    StateSettingsMargin,
    StateSettingsRPM,

    StateManualDelta,
    StateManualPosition,

    StateGuidedCutWidth,
    StateGuidedStart,
    StateGuidedStop,
    StateGuidedMove,

    NUM_STATES
} state_t;

extern const state_t menuReturn[];

typedef int (*stringer_t)(char * buf, int n);

struct state_table {
    const char header[15];
    state_t (*fn)(void * data, int turn, int press);
    void * data;
};

typedef struct {
    const state_t state;
    const char * display;
    stringer_t displayf;
} menu_item_t;

typedef struct menu {
    const menu_item_t * items;
    const int count;
    int selected;
} menu_t;

template<typename T> struct mutate_args {
    menu_t * menu;
    void (*on_press)();
    void (*mutate)(int turn);
};

#endif