#include "main.h"

extern stringer_t string_bladeKerf, string_margin, string_rpm, string_posDelta, string_actualPos, string_cutWidth, string_guidedPos, string_actualPosG, string_targetPosG;

const char * back = "\x80 Back";

const state_t menuReturn[NUM_STATES] = {
    SameState,

    StateMain,
    StateMain,
    StateMain,

    StateSettings,
    StateSettings,
    StateSettings,

    StateManual,
    StateManual,

    StateGuided,
    StateGuided,
    StateGuided,
    StateGuided,
};

#define MENU(name) menu_t name##Menu = { .items = _##name##MenuItems, .count = LEN(_##name##MenuItems) }

const menu_item_t _mainMenuItems[] = {
    {StateSettings, "Settings     \x81", NULL},
    {StateManual,   "Manual       \x81", NULL},
    {StateGuided,   "Guided       \x81", NULL},
};
MENU(main);

const menu_item_t _settingsMenuItems[] = {
    {ReturnState,            back,         NULL},
    {StateSettingsBladeKerf, "Blade Kerf", string_bladeKerf},
    {StateSettingsMargin,    "Margin",     string_margin},
    {StateSettingsRPM,       "RPM",        string_rpm},
};
MENU(settings);

const menu_item_t _manualMenuItems[] = {
    {ReturnState,         back,       NULL},
    {StateManualDelta,    "Delta",    string_posDelta},
    {StateManualPosition, "Position", string_actualPos},
};
MENU(manual);

const menu_item_t _guidedOffMenuItems[] = {
    {ReturnState,         back,                NULL},
    {StateGuidedCutWidth, "Width",             string_cutWidth},
    {StateGuidedStart,    "Start        \x81", NULL},
};
MENU(guidedOff);

const menu_item_t _guidedOnMenuItems[] = {
    {StateGuidedStop, "\x80 Stop", NULL},
    {StateGuidedMove, "Pass",      string_guidedPos },
    {SameState,       "Position",  string_actualPosG },
    {SameState,       "Target",    string_targetPosG },
};
MENU(guidedOn);