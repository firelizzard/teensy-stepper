#ifndef Stepper_h
#define Stepper_h

class Stepper
{
public:
    Stepper(int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm);

    void brake();
    void standby();
    void stop();
    void step(int pos);

private:
    int stby;
    int apwm, ain2, ain1;
    int bin1, bin2, bpwm;
};

#endif