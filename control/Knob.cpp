#include "Knob.h"

// CW down, CCW up
//
// A B
// 0 1
// 0 0
// 1 0
// 1 1

// XAYB where AB are current and XY are previous
const int Knob::states[16] = {
    0,       // 0000
    DIR_CCW, // 0001
    DIR_CW,  // 0010
    0,       // 0011
    DIR_CW,  // 0100
    0,       // 0101
    0,       // 0110
    DIR_CCW, // 0111
    DIR_CCW, // 1000
    0,       // 1001
    0,       // 1010
    DIR_CW,  // 1011
    0,       // 1100
    DIR_CW,  // 1101
    DIR_CCW, // 1110
    0,       // 1111
};

// states[0b1011] = DIR_CW;
// states[0b0010] = DIR_CW;
// states[0b0100] = DIR_CW;
// states[0b1101] = DIR_CW;
// states[0b1110] = DIR_CCW;
// states[0b1000] = DIR_CCW;
// states[0b0001] = DIR_CCW;
// states[0b0111] = DIR_CCW;

Knob::Knob(int button, int channel_a, int channel_b)
{
    pinMode(button, INPUT_PULLUP);
    pinMode(channel_a, INPUT_PULLUP);
    pinMode(channel_b, INPUT_PULLUP);

    this->btn = button;
    this->cha = channel_a;
    this->chb = channel_b;

    this->readEncoder(); // initialize state
}

void Knob::onSwitch(void (*fn)(void))
{
    attachInterrupt(btn, fn, RISING);
}

void Knob::onEncoder(void (*fn)(void))
{
    attachInterrupt(cha, fn, CHANGE);
    attachInterrupt(chb, fn, CHANGE);
}

int Knob::readSwitch()
{
    return digitalRead(btn);
}

int Knob::readEncoder()
{
    int a = digitalRead(cha);
    int b = digitalRead(chb);
    return updateEncoder(a, b);
}

int Knob::updateEncoder(int a, int b)
{
    // shift last state, clear next state, apply next state
    state = ((state << 1) & 0b1010) | a << 2 | b;
    return states[state];
}

TurnDecoder::TurnDecoder(Knob *knob)
{
    this->knob = knob;
}

int TurnDecoder::read()
{
    return substep(knob->readEncoder());
}

int TurnDecoder::update(int a, int b)
{
    return substep(knob->updateEncoder(a, b));
}

int TurnDecoder::substep(int dir)
{
    if (!dir) return 0;

    substeps += dir;
    _s = substeps;
    if (substeps == 4) {
        substeps = 0;
        return +1;
    } else if (substeps == -4) {
        substeps = 0;
        return -1;
    } else {
        return 0;
    }
}

int TurnDecoder::getSubsteps()
{
    return _s;
}