#include "Stepper.h"
#include <Arduino.h>

Stepper::Stepper(int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm)
{
    this->apwm = apwm;
    this->ain2 = ain2;
    this->ain1 = ain1;
    this->stby = standby;
    this->bin1 = bin1;
    this->bin2 = bin2;
    this->bpwm = bpwm;

    pinMode(apwm, OUTPUT);
    pinMode(ain2, OUTPUT);
    pinMode(ain1, OUTPUT);
    pinMode(standby, OUTPUT);
    pinMode(bin1, OUTPUT);
    pinMode(bin2, OUTPUT);
    pinMode(bpwm, OUTPUT);

    digitalWrite(standby, LOW);
    digitalWrite(apwm, HIGH);
    digitalWrite(bpwm, HIGH);
}

void Stepper::standby() {
    digitalWrite(stby, LOW);
}

void Stepper::brake() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, HIGH);
    digitalWrite(ain2, HIGH);
    digitalWrite(bin1, HIGH);
    digitalWrite(bin2, HIGH);
}

void Stepper::stop() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, LOW);
    digitalWrite(ain2, LOW);
    digitalWrite(bin1, LOW);
    digitalWrite(bin2, LOW);
}

void Stepper::step(int pos) {
    // DUAL
    // -------
    // N | A B
    // 0 | < <
    // 1 | > <
    // 2 | > >
    // 3 | < >
    digitalWrite(ain1, pos == 0 || pos == 1);
    digitalWrite(bin1, pos == 1 || pos == 2);
    digitalWrite(ain2, pos == 2 || pos == 3);
    digitalWrite(bin2, pos == 3 || pos == 0);
    digitalWrite(stby, HIGH);
}
