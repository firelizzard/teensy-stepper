#ifndef Knob_h
#define Knob_h

#include <Arduino.h>

const int DIR_CW  = -1;
const int DIR_CCW = +1;
const int DIR_INV =  0;

class Knob
{
public:
    Knob(int button, int channel_a, int channel_b);

    void onSwitch(void (*fn)(void));
    void onEncoder(void (*fn)(void));

    int readSwitch();
    int readEncoder();

    int updateEncoder(int a, int b);

private:
    static const int states[16];

    int btn, cha, chb;
    int state = 0;
};

class TurnDecoder
{
public:
    TurnDecoder(Knob *knob);

    int read();
    int getSubsteps();

    int update(int a, int b);

private:
    Knob *knob;
    int _s, substeps = 0;

    int substep(int dir);
};

#endif