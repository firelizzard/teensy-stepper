#ifndef Screen_h
#define Screen_h

#include <Arduino.h>
#include <stdint.h>
#include <SPI.h>

class ScreenControl
{
public:
    ScreenControl(int mode, int rst, int sce);

    void begin(bool cmd);
    virtual void send(const char v) = 0;
    void end();

    void reset();
    void write(bool cmd, const char * data);
    void write(bool cmd, const char * data, int n);
    void fill(const char v, int n);

protected:
    static const int MODE_CMD = LOW, MODE_DATA = HIGH;

private:
    int mode, rst, sce;
};

class ScreenControlShift : public ScreenControl
{
public:
    ScreenControlShift(int sck, int mosi, int mode, int rst, int sce);
    void send(const char v) override;

private:
    int sck, mosi;
};

class ScreenControlSPI : public ScreenControl
{
public:
    ScreenControlSPI(SPIClass * spi, int mode, int rst, int sce);
    void send(const char v) override;

private:
    SPIClass * spi;
};

class Screen
{
public:
    static const int WIDTH = 84, HEIGHT = 48;
    static const int CHAR_WIDTH = 6;
    static const char MIN_CHAR = ' ';
    static const char MAX_CHAR = '\x82';
    static const char CHAR_COUNT = MAX_CHAR - MIN_CHAR + 1;

    Screen(ScreenControl * ctrl);

    void begin();
    void clear();
    void display(uint8_t x, uint8_t y, const char *data, int n);
    void print(uint8_t x, uint8_t y, const char *data, int n, bool invert);

private:
    static const char ASCII[CHAR_COUNT][CHAR_WIDTH];

    ScreenControl * ctrl;

    void setPos(uint8_t x, uint8_t y);
    void msgSendASCII(const char c, bool invert);
};

#define screen_snprintf(screen, x, y, invert, n, fmt, ...) \
    do { \
        char s[n+1] = {0}; \
        int l = snprintf(s, n+1, fmt, __VA_ARGS__); \
        screen.print(x, y, s, -1, invert); \
    } while (0)

#endif