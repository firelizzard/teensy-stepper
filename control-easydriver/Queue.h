#ifndef Queue_h
#define Queue_h

#include <Arduino.h>

template <typename T, int DEPTH>
class Queue
{
public:
    void push(T item);
    bool pop(T * item);

private:
    volatile int head = 0, tail = 0, live = 0;
    T events[DEPTH];
};

#define increment(v) (v = (v + 1) % DEPTH)

template <typename T, int DEPTH>
void Queue<T, DEPTH>::push(T item)
{
    noInterrupts();

    events[head] = item;
    increment(head);
    if (live < DEPTH)
        live++;
    else
        tail = head;

    interrupts();
}

template <typename T, int DEPTH>
bool Queue<T, DEPTH>::pop(T * item)
{
    noInterrupts();
    if (!live) {
        interrupts();
        return false;
    }

    *item = events[tail];
    increment(tail);
    live--;

    interrupts();
    return true;
}

#undef increment

#endif