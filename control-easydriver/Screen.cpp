#include "Screen.h"

#define LEN(x) sizeof(x)/sizeof(x[0])
#define POS_X(x) ((char)(0x80|x))
#define POS_Y(y) ((char)(0x40|y))

#define MSG_B(cmd) do { ctrl->begin(cmd)
#define MSG_E() ctrl->end(); } while(0)

Screen::Screen(ScreenControl * ctrl)
{
    this->ctrl = ctrl;
}

void Screen::begin()
{
    static const char seq1[] = {
		0x20,     // Normal Commands
		POS_X(0), // X => 0
		POS_Y(0), // Y => 0
    };

    static const char seq2[] = {
		0x21, // Extended Commands
		0xBF, // Contrast (Vop)
		0x04, // Temperature Coefficient
		0x14, // Bias Mode 1:48
		0x20, // Normal Commands
		0x08, // Blank Display
    };

    static const char seq3[] = {
		0x09, // Black Display
    };

    static const char seq4[] = {
		0x0C, // Display Mode (0x0C => Normal, 0x0D => Inverted)
    };

    ctrl->reset();

    ctrl->write(true, seq1, LEN(seq1));

    clear();

	ctrl->write(true, seq2, LEN(seq2));

	delay(250);

	ctrl->write(true, seq3, LEN(seq3));

	delay(250);

	ctrl->write(true, seq4, LEN(seq4));
}

void Screen::clear()
{
    ctrl->fill(0, WIDTH * HEIGHT / 8);
}

void Screen::display(uint8_t x, uint8_t y, const char *data, int n)
{
    if (!data || !n || x >= WIDTH || y >= HEIGHT / 8) return;

    setPos(x, y);
    ctrl->write(false, data, n);
}

void Screen::print(uint8_t x, uint8_t y, const char *data, int n, bool invert)
{
    if (!data || !n || x >= WIDTH || y >= HEIGHT / 8) return;

    // if (n < 0)
    //     Serial.printf("Print (%d, %d) -> '%s'\n", x, y, data);
    // else
    //     Serial.printf("Print (%d, %d) -> '%.*s'\n", x, y, n, data);

    setPos(x, y);

    MSG_B(false);

    for (int i = 0; (n > 0 && i < n) || data[i]; i++)
        msgSendASCII(data[i], invert);

    MSG_E();
}

void Screen::msgSendASCII(const char c, bool invert)
{
    if (c < MIN_CHAR || c > MAX_CHAR) {
        msgSendASCII('?', !invert);
        return;
    }

    const char * block = &ASCII[c-' '][0];

    for (int i = 0; i < CHAR_WIDTH; i++)
        if (invert)
            ctrl->send(~block[i]);
        else
            ctrl->send(block[i]);
}

void Screen::setPos(uint8_t x, uint8_t y)
{
    char cmds[] = {
        POS_X(x),
        POS_Y(y),
    };
    ctrl->write(true, cmds, LEN(cmds));
}
