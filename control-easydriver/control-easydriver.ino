#include "Knob.h"
#include "Screen.h"
#include "Queue.h"
#include "Stepper.h"

#include "main.h"

// EasyDriver pins
constexpr int DrvDir = 5;
constexpr int DrvStep = 6;
constexpr int DrvMode1 = 24;
constexpr int DrvMode2 = 10;
constexpr int DrvEn = 12;

// Screen pins
constexpr int ScrClk = 13;
constexpr int ScrData = 11;
constexpr int ScrMode = 7;
constexpr int ScrRst = 8;
constexpr int ScrEn = 9;

// Encoder/Switch pins
constexpr int KnobA = 2;
constexpr int KnobB = 3;
constexpr int KnobSw = 4;

// Threads per inch
constexpr int RevPerInch = 18;

// Steps per revolution
constexpr int StepsPerRev = 200;

// Microstepping Mode
//   0 - full step
//   1 - half step
//   2 - quarter step
//   3 - eigth step
constexpr int MicroStep = 0;

constexpr int Polarity = +1;

constexpr int StepsPer16th = StepsPerRev * RevPerInch / 16;

IntervalTimer stepTimer;
ScreenControlSPI sctrl(&SPI, ScrMode, ScrRst, ScrEn);
Screen screen(&sctrl);
Knob knob(KnobSw, KnobA, KnobB);
TurnDecoder turnd(&knob);

#define floor(v, lb) { \
        if (v < lb) v = lb; \
    }

#define range(v, lb, ub) { \
        if (v < lb) v = lb; \
        else if (v > ub) v = ub; \
    }

#undef round
#define round(v, n) { \
        int r = v % n; \
        if (r) { \
            if (r < n/2) v -= r; \
            else v += n-r; \
        } \
    }

constexpr int posDeltaConv[] = { 1, StepsPerRev/20, StepsPerRev/4, StepsPerRev, StepsPerRev*2 };

struct Settings {
    static constexpr unsigned int Magic = 0xDEADBEEF;

    unsigned int magic;
    int bladeKerf, margin, rpm;

    float interval() { return 60. * 1000000 / StepsPerRev / (1 << MicroStep) / (rpm <= 0 ? 1 : rpm); }

    void dBladeKerf(int d) { bladeKerf += d; floor(bladeKerf, 0); }
    void dMargin(int d) { margin += d; }
    void dRPM(int d) { rpm += d * 10; floor(rpm, 10); round(rpm, 10); }

    Settings() {
        eeprom_read_block((void *)this, 0, sizeof(struct Settings));
        if (magic == Magic) return;

        magic = Magic;
        bladeKerf = 0;
        margin = 0;
        rpm = 20;
    }

    void write() {
        eeprom_write_block((void *)this, 0, sizeof(struct Settings));
    }

    void updateRPM() {
        stepTimer.update(interval());
        write();
    }
} settings;

struct Current {
    static const int maxPosDelta = LEN(posDeltaConv) - 1;

    bool guidedActive;
    int posDelta, cutWidth;
    int guidedPos, guidedStartPos, cutWidthSteps, passesPerCut;
    volatile int targetPos, actualPos;

    int convPosDelta() { return posDeltaConv[posDelta]; }

    void dPosDelta(int d) { posDelta += d; range(posDelta, 0, maxPosDelta); }
    void dTargetPos(int d) { targetPos += d * convPosDelta(); }
    void dCutWidth(int d) { cutWidth += d; floor(cutWidth, 1); }

    void dTargetPosInch(int d) {
        if (d == 0) return;

        int delta = (d < 0 ? -d : d) * StepsPer16th;
        int remainder = (targetPos < 0 ? -targetPos : targetPos) % StepsPer16th;

        if ((d < 0) == (targetPos < 0))
            delta -= remainder; // same sign
        else if (remainder > 0)
            delta -= StepsPer16th - remainder; // different sign

        targetPos += d < 0 ? -delta : delta;
    }

    void dGuidedPos(int d) {
        guidedPos += d;
        floor(guidedPos, 0);

        int pos = guidedStartPos;
        if (cutWidth == 1)
            pos += guidedPos * cutWidthSteps * 2 + settings.margin;
        else {
            int slotNum = guidedPos / passesPerCut;
            int passNum = guidedPos % passesPerCut;

            pos += slotNum * cutWidthSteps * 2;

            if (passNum == passesPerCut - 1)
                pos += cutWidthSteps - settings.bladeKerf - settings.margin;
            else
                pos += passNum * settings.bladeKerf;
        }

        targetPos = pos;
    }

    Current() {
        posDelta = 2;
        targetPos = 0;
        actualPos = 0;
        cutWidth = 1;
        guidedActive = false;
    }

    bool step() {
        static int last_dir = 0;

        if (actualPos == targetPos) {
            // stepper.brake();
            return false;
        }

        int dir;
        if (actualPos < targetPos) {
            dir = +1;
            actualPos++;
        } else {
            dir = -1;
            actualPos--;
        }

        if (last_dir != dir) {
            // set direction
            digitalWrite(DrvDir, dir * Polarity > 0);

            // A3967 min setup time = 0.2µs
            delayMicroseconds(1);
        }

        digitalWrite(DrvStep, HIGH);
        return true;
    }

    void zeroPosition() {
        noInterrupts();
        if (actualPos == targetPos)
            actualPos = targetPos = 0;
        interrupts();
    }

    void startGuiding() {
        guidedActive = true;
        guidedPos = 0;
        guidedStartPos = targetPos;

        if (cutWidth <= 1) {
            cutWidthSteps = settings.bladeKerf;
            passesPerCut = 1;

        } else {
            cutWidthSteps = cutWidth * StepsPer16th;
            passesPerCut = cutWidthSteps / settings.bladeKerf;
            if (cutWidthSteps % settings.bladeKerf) passesPerCut++;
        }

        // Serial.printf("Guiding with %d steps/cut, %d passes/cut\n", cutWidthSteps, passesPerCut);
    }

    void stopGuiding() {
        guidedActive = false;
    }
} current;

bool isGuiding() { return current.guidedActive; }
void startGuiding() { current.startGuiding(); }
void stopGuiding() { current.stopGuiding(); }
void posSetZero() { current.zeroPosition(); }

int fmt_inch(int value, char * buf, int n) {
    constexpr char empty[] = "";
    constexpr char neg[] = "-";
    constexpr char inch[] = "\"";
    constexpr char aprox[] = "~";

    if (value == 0)
        return snprintf(buf, n, "0\"");

    const char * s = &empty[0];
    if (value < 0) {
        s = &neg[0];
        value = -value;
    }

    const char * r = &inch[0];
    if (value % StepsPer16th > 0) {
        r = &aprox[0];
    }

    int v16ths = value / StepsPer16th;
    if (v16ths == 0)
        return snprintf(buf, n, "%s0%s", s, r);

    switch (__builtin_ctz(v16ths)) {
        case 0: return snprintf(buf, n, "%s%d/16%s", s, v16ths >> 0, r);
        case 1: return snprintf(buf, n, "%s%d/ 8%s", s, v16ths >> 1, r);
        case 2: return snprintf(buf, n, "%s%d/ 4%s", s, v16ths >> 2, r);
        case 3: return snprintf(buf, n, "%s%d/ 2%s", s, v16ths >> 3, r); default: // fallthrough
        case 4: return snprintf(buf, n, "%s%d   %s", s, v16ths >> 4, r);
    }
}

#define fmt_fn [](char * buf, int n)
#define fmt_menu(v, fmt) fmt_fn { return snprintf(buf, n, fmt, v); }

constexpr char pos_int_fmt[] = "%d";
constexpr char int_fmt[] = "%+d";

stringer_t string_bladeKerf     = fmt_menu(settings.bladeKerf, pos_int_fmt);
stringer_t string_margin        = fmt_menu(settings.margin, int_fmt);
stringer_t string_rpm           = fmt_menu(settings.rpm, pos_int_fmt);
stringer_t string_posDelta      = fmt_menu(current.convPosDelta(), pos_int_fmt);
stringer_t string_actualPos     = fmt_menu(current.actualPos, pos_int_fmt);
stringer_t string_targetPos     = fmt_menu(current.targetPos, pos_int_fmt);
stringer_t string_actualPosInch = fmt_fn { return fmt_inch(current.actualPos, buf, n); };
stringer_t string_targetPosInch = fmt_fn { return fmt_inch(current.targetPos, buf, n); };
stringer_t string_actualPosG    = fmt_menu(current.actualPos - current.guidedStartPos, pos_int_fmt);
stringer_t string_targetPosG    = fmt_menu(current.targetPos - current.guidedStartPos, pos_int_fmt);

stringer_t string_cutWidth = fmt_fn {
    if (current.cutWidth == 1)
        return snprintf(buf, n, "kerf");
    switch (__builtin_ctz(current.cutWidth)) {
        case 0: return snprintf(buf, n, "%d/16\"", current.cutWidth);
        case 1: return snprintf(buf, n, "%d/ 8\"", current.cutWidth >> 1);
        case 2: return snprintf(buf, n, "%d/ 4\"", current.cutWidth >> 2);
        case 3: return snprintf(buf, n, "%d/ 2\"", current.cutWidth >> 3); default: // fallthrough
        case 4: return snprintf(buf, n, "%d   \"", current.cutWidth >> 4);
    }
};

stringer_t string_guidedPos = fmt_fn {
    if (current.cutWidth == 1)
        return snprintf(buf, n, "#%d", current.guidedPos + 1);

    int slotNum = current.guidedPos / current.passesPerCut;
    int passNum = current.guidedPos % current.passesPerCut;
    return snprintf(buf, n, "#%d, %d/%d", slotNum+1, passNum+1, current.passesPerCut);
};

#define call(field)   [] { field(); }
#define mutate(field) [](int turn) { field(turn); }

struct mutate_args<int> mutate_bladeKerf     = {&settingsMenu,  call(settings.write),     mutate(settings.dBladeKerf   )};
struct mutate_args<int> mutate_margin        = {&settingsMenu,  call(settings.write),     mutate(settings.dMargin      )};
struct mutate_args<int> mutate_rpm           = {&settingsMenu,  call(settings.updateRPM), mutate(settings.dRPM         )};
struct mutate_args<int> mutate_posDelta      = {&manualMenu,    NULL,                     mutate(current.dPosDelta     )};
struct mutate_args<int> mutate_targetPos     = {&manualMenu,    NULL,                     mutate(current.dTargetPos    )};
struct mutate_args<int> mutate_targetPosInch = {&manualMenu,    NULL,                     mutate(current.dTargetPosInch)};
struct mutate_args<int> mutate_cutWidth      = {&guidedOffMenu, NULL,                     mutate(current.dCutWidth     )};
struct mutate_args<int> mutate_guidedPos     = {&guidedOnMenu,  NULL,                     mutate(current.dGuidedPos    )};

bool wantsEncoderUpdates(state_t state);

typedef enum {
    kLifeEvent,
    kSwitchEvent,
    kEncoderEvent,
    kStepperEvent,

    kDebugEvent
} event_source_t;

typedef struct {
    event_source_t source;
    int value;
    unsigned long micros;
} event_t;

Queue<event_t, 256> events;

#define push_event(...) do { event_t e = { __VA_ARGS__ }; events.push(e); } while(0);

void buttonISR() {
    static unsigned long then = 0;
    unsigned long now = micros();

    // if (then == 0 || now - then < 1000)
        push_event(kSwitchEvent, 0, now);

    // then = now;
}

void encoderISR() {
    unsigned long now = micros();
    int turn = turnd.read();
    if (!turn) return;

    push_event(kEncoderEvent, turn, now);
}

void step() {
    static int last_event = 0, last_pos = current.actualPos;

    bool moved = current.step();

    // send an event if A) we moved and B) we reached the target or it's been 250ms since the last event
    int now = millis();
    if (last_pos != current.actualPos && (!moved || now - last_event > 250)) {
        last_event = now;
        last_pos = current.actualPos;
        push_event(kStepperEvent, current.actualPos, micros());
    }

    // A3967 min pulse width = 1µs
    delayMicroseconds(5);
    digitalWrite(DrvStep, LOW);
}

int main() {
    static auto state = StateMain;

    SPI.setSCK(ScrClk);
    SPI.setMOSI(ScrData);
    SPI.begin();

    Serial.begin(115200);
    screen.begin();

    if (!stepTimer.begin(step, settings.interval()))
        Serial.println("Failed to start timer");

#if HAS_DEBUGGER
    debug.begin();
#endif

    knob.onSwitch(buttonISR);
    knob.onEncoder(encoderISR);

    pinMode(DrvDir, OUTPUT);
    pinMode(DrvStep, OUTPUT);
    pinMode(DrvMode1, OUTPUT);
    pinMode(DrvMode2, OUTPUT);
    pinMode(DrvEn, OUTPUT);

    digitalWrite(DrvMode1, MicroStep & 0x1);
    digitalWrite(DrvMode2, MicroStep & 0x2);
    digitalWrite(DrvDir, LOW);
    digitalWrite(DrvStep, LOW);
    digitalWrite(DrvEn, LOW);

    push_event(kLifeEvent, 0, 0);

    for (;;) {
        event_t e = {};
        if (!events.pop(&e)) {
            delay(1);
            continue;
        }

        // if (Serial) {
        //     if (e.source == kDebugEvent)
        //         Serial.printf("Debug -> %d\r\n", e.value);
        //     else
        //         Serial.printf("Event -> %d, %+d\r\n", e.source, e.value);
        // }

        static unsigned long last_press = 0;
        if (e.source == kSwitchEvent) {
            // Serial.printf("Click: %luus\n", e.micros - last_press);
            if (last_press != 0 && e.micros - last_press < 1000) {
                last_press = e.micros;
                continue;
            }
            last_press = e.micros;
        }

        int turn = 0, press = 0;
        switch (e.source) {
            case kLifeEvent:                    break;
            case kSwitchEvent:  press = 1;      break;
            case kEncoderEvent: turn = e.value; break;
            default: continue;

            case kStepperEvent:
                // only update if we're on a screen that shows the stepper position
                if (!wantsEncoderUpdates(state)) continue;
                break;
        }

        if (e.source == kEncoderEvent && !turn) continue;

        if (state < 0 || state >= NUM_STATES) {
            Serial.printf("Bad state: %d\r\n", state);
            state = StateMain;
        }

        auto * s = &states[state];

        screen.clear();
        screen.print(0, 0, s->header, -1, true);

        state_t next = s->fn(s->data, turn, press);
        if (next == SameState) continue;

        if (next == ReturnState)
            state = menuReturn[state];
        else
            state = next;

        // Serial.printf("State -> %d\r\n", state);
        push_event(kLifeEvent, 0, 0);
    }

    return 0;
}
