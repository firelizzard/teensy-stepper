#include "Screen.h"

#define MSG_B(cmd) do { begin(cmd)
#define MSG_E() end(); } while(0)

ScreenControl::ScreenControl(int mode, int rst, int sce)
{
    this->mode = mode;
    this->rst  = rst;
    this->sce  = sce;

    pinMode(mode, OUTPUT);
    pinMode(rst, OUTPUT);
    pinMode(sce, OUTPUT);
}

void ScreenControl::reset()
{
    digitalWrite(rst, LOW);
    // delay(1);
    digitalWrite(rst, HIGH);
}

void ScreenControl::begin(bool cmd)
{
    digitalWrite(mode, cmd ? MODE_CMD : MODE_DATA);
    digitalWrite(sce, LOW);
}

void ScreenControl::end()
{
    digitalWrite(sce, HIGH);
}

void ScreenControl::write(bool cmd, const char * data)
{
    if (!data) return;

    MSG_B(cmd);

    for (; *data; data++)
        send(*data);

    MSG_E();
}

void ScreenControl::write(bool cmd, const char * data, int n)
{
    if (n < 0) {
        write(cmd, data);
        return;
    }

    if (!data || !n) return;

    MSG_B(cmd);

    for (int i = 0; i < n; i++)
        send(data[i]);

    MSG_E();
}

void ScreenControl::fill(const char v, int n)
{
    MSG_B(false);

    for (; n > 0; n--)
        send(v);

    MSG_E();
}

ScreenControlShift::ScreenControlShift(int sck, int mosi, int mode, int rst, int sce)
    : ScreenControl(mode, rst, sce)
{
    this->sck = sck;
    this->mosi = mosi;

    pinMode(sck, OUTPUT);
    pinMode(mosi, OUTPUT);
}

void ScreenControlShift::send(const char v)
{
    shiftOut(mosi, sck, MSBFIRST, v);
}

ScreenControlSPI::ScreenControlSPI(SPIClass * spi, int mode, int rst, int sce)
    : ScreenControl(mode, rst, sce)
{
    this->spi = spi;
}

void ScreenControlSPI::send(const char v)
{
    spi->transfer(v);
}
