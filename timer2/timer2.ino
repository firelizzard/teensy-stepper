#include "Stepper.h"
#include <Wire.h>
#include <Adafruit_BusIO_Register.h>
#include <Adafruit_I2CDevice.h>

volatile int steps = 0;
volatile int cur_pos = 0;

volatile bool debug = false;

const int rpm = 20;

IntervalTimer stepTimer;
Stepper stepper(14, 15, 16, 17, 18, 19, 20);

FASTRUN void step()
{
    if (steps > 0) {
        steps--;
        cur_pos = (cur_pos + 1) % 200;
    } else if (steps < 0) {
        steps++;
        cur_pos = (cur_pos ? cur_pos : 200) - 1;
    } else {
        stepper.brake();
        return;
    }

    stepper.step(cur_pos % 4);
    debug = true;
}

void setup() {
    Serial.begin(115200);

    if (!stepTimer.begin(step, 60. * 1000000 / 200 / rpm)) {
        Serial.println("Failed to start timer");
    }
}

void loop() {
    if (debug) {
        Serial.printf("%d\t", cur_pos);
        stepper.debug(cur_pos % 4);
        debug = false;
    }

    if (!Serial.available()) return;
    int b = Serial.read();
    switch (b) {
    case 0x1b:
        break;

    default:
        Serial.println(b, HEX);
        return;
    }

    while (!Serial.available()) {}
    b = Serial.read();
    if (b != 0x5b) {
        Serial.println(b, HEX);
        return;
    }

    while (!Serial.available()) {}
    b = Serial.read();
    switch (b) {
        case 'C':
            noInterrupts();
            steps += 200;
            interrupts();
            break;
        case 'D':
            noInterrupts();
            steps -= 200;
            interrupts();
            break;
        default:
            Serial.printf("^[%c\r\n", b);
            break;
    }

}