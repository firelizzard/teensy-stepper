#ifndef Stepper_h
#define Stepper_h

class Stepper
{
public:
    Stepper(int steps_per_rev, int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm);
    
    void brake();
    void standby();
    void stop();

    void stepSolo(bool fwd);
    void stepDual(bool fwd);
    void stepHalf(bool fwd, int delay);
    void stepMicro(bool fwd, int micros, int delay);

private:
    int increment(bool fwd);
    void position(int pos, int mode);
    void transition(int pos, int i);
    void prepare();
    void set();
    void debug(int pos, int i);

    int stby;
    int apwm, ain2, ain1;
    int bin1, bin2, bpwm;
    int N, i;

    int ain1_state, ain2_state, apwm_state, bin1_state, bin2_state, bpwm_state;
};

#endif