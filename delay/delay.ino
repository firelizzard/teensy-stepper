#include "Stepper.h"
#include <Wire.h>
#include <Adafruit_BusIO_Register.h>
#include <Adafruit_I2CDevice.h>

const int APWM = 14;
const int AIN2 = 15;
const int AIN1 = 16;
const int STBY = 17;
const int BIN1 = 18;
const int BIN2 = 19;
const int BPWM = 20;

int fineness = 2;

Stepper stepper(200, APWM, AIN2, AIN1, STBY, BIN1, BIN2, BPWM);

void setup() {
    Serial.begin(115200);
}

void loop() {
    while (!Serial.available()) {}
    int b = Serial.read();
    switch (b) {
    case 0x1b:
        break;

    default:
        Serial.println(b, HEX);
        return;
    }

    while (!Serial.available()) {}
    b = Serial.read();
    if (b != 0x5b) {
        Serial.println(b, HEX);
        return;
    }

    while (!Serial.available()) {}
    b = Serial.read();
    switch (b) {
        case 'A':
            step(+5, 10);
            break;
        case 'B':
            step(-5, 10);
            break;
        case 'C':
            if (++fineness > 5) {
                fineness = 5;
            }
            break;
        case 'D':
            if (--fineness < 0) {
                fineness = 0;
            }
            break;
        default:
            Serial.printf("^[%c\r\n", b);
            break;
    }

}

void step(int count, int rpm)
{
    int micro_per_step = 60 * 1000000 / 200 / rpm;
    bool fwd = count >= 0;
    if (!fwd) count *= -1;

    Serial.printf("%c%d steps @ %d rpm\r\n", fwd ? '+' : '-', count, rpm);

    for (; count > 0; count--)
        stepper.stepMicro(fwd, fineness, micro_per_step);

    stepper.brake();
}