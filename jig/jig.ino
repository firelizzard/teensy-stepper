#include "Stepper.h"
#include <Wire.h>
#include <Adafruit_BusIO_Register.h>
#include <Adafruit_I2CDevice.h>

const int APWM = 14;
const int AIN2 = 15;
const int AIN1 = 16;
const int STBY = 17;
const int BIN1 = 18;
const int BIN2 = 19;
const int BPWM = 20;

const int revs_per_inch = 18;
const int steps_per_rev = 200;
const int rpm = 60;

const int blade_width_steps = 200 * 18 * 0.071;
const int steps_per_finger = 200 * 18 * 0.25;

Stepper stepper(200, APWM, AIN2, AIN1, STBY, BIN1, BIN2, BPWM);
Adafruit_I2CDevice sensor(0x40, &Wire);
Adafruit_BusIO_Register sensor_reg_config (&sensor, 0x0, 2, MSBFIRST);
Adafruit_BusIO_Register sensor_reg_shunt_v(&sensor, 0x1, 2, MSBFIRST);
Adafruit_BusIO_Register sensor_reg_bus_v  (&sensor, 0x2, 2, MSBFIRST);
Adafruit_BusIO_Register sensor_reg_power  (&sensor, 0x3, 2, MSBFIRST);
Adafruit_BusIO_Register sensor_reg_current(&sensor, 0x4, 2, MSBFIRST);
Adafruit_BusIO_Register sensor_reg_cal    (&sensor, 0x5, 2, MSBFIRST);

int steps_remaining = 0;

void setup() {
    Serial.begin(115200);
    steps_remaining = steps_per_finger - blade_width_steps;

    if (!sensor.begin()) {
        Serial.println("Failed to initialize sensor");
    } else {
        sensor_reg_cal.write(4096, 2);

        // 0-32V Range; Gain 8, 320mV Range; 12-bit Bus Res; 1 x 12-bit Shunt Sample; Bus Voltage Continuous
        sensor_reg_config.write(0x2000 | 0x1800 | 0x0180 | 0x0018 | 0x06, 2);
    }
}

void loop() {
    while (!Serial.available()) {}
    int b = Serial.read();

    switch (b) {
    case 0x20:
        if (steps_remaining > blade_width_steps) {
            step(blade_width_steps, rpm);
            steps_remaining -= blade_width_steps;
        } else if (steps_remaining) {
            step(steps_remaining, rpm);
            steps_remaining = 0;
        }
        break;

    case 0x0D:
        if (steps_remaining) break;
        steps_remaining = steps_per_finger - blade_width_steps;
        step(steps_per_finger, rpm);
        break;

    default:
        Serial.println(b, HEX);
        return;
    }
}

void step(int count, int rpm)
{
    int micro_per_step = 60 * 1000000 / 200 / rpm;
    bool fwd = count >= 0;
    if (!fwd) count *= -1;

    Serial.printf("%c%d steps @ %d rpm\r\n", fwd ? '+' : '-', count, rpm);

    for (; count > 0; count--)
        stepper.stepMicro(fwd, 5, micro_per_step);

    stepper.brake();
}