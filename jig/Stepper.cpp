#include "Stepper.h"
#include <Arduino.h>

#define SOLO 1
#define DUAL 2
#define DEBUG

const int sine_table[] = {0,13,25,37,50,62,74,86,98,109,120,131,142,152,162,171,180,189,197,205,212,219,225,231,236,240,244,247,250,252,254,255,255};

Stepper::Stepper(int steps_per_rev, int apwm, int ain2, int ain1, int standby, int bin1, int bin2, int bpwm)
{
    this->apwm = apwm;
    this->ain2 = ain2;
    this->ain1 = ain1;
    this->stby = standby;
    this->bin1 = bin1;
    this->bin2 = bin2;
    this->bpwm = bpwm;

    this->N = steps_per_rev;
    this->i = 0;

    pinMode(apwm, OUTPUT);
    pinMode(ain2, OUTPUT);
    pinMode(ain1, OUTPUT);
    pinMode(standby, OUTPUT);
    pinMode(bin1, OUTPUT);
    pinMode(bin2, OUTPUT);
    pinMode(bpwm, OUTPUT);

    digitalWrite(standby, LOW);
    digitalWrite(apwm, HIGH);
    digitalWrite(bpwm, HIGH);
}

void Stepper::standby() {
    digitalWrite(stby, LOW);
}

void Stepper::brake() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, HIGH);
    digitalWrite(ain2, HIGH);
    digitalWrite(bin1, HIGH);
    digitalWrite(bin2, HIGH);
}

void Stepper::stop() {
    digitalWrite(stby, HIGH);
    digitalWrite(ain1, LOW);
    digitalWrite(ain2, LOW);
    digitalWrite(bin1, LOW);
    digitalWrite(bin2, LOW);
}

void Stepper::set() {
    digitalWrite(ain1, ain1_state);
    digitalWrite(ain2, ain2_state);
    digitalWrite(bin1, bin1_state);
    digitalWrite(bin2, bin2_state);
    analogWrite(apwm, apwm_state);
    analogWrite(bpwm, bpwm_state);

#ifdef DEBUG
    Serial.printf("A %3d {%d %d}\tB %3d {%d %d}\t[%d]\r\n", apwm_state, ain1_state, ain2_state, bpwm_state, bin1_state, bin2_state, this->i % 4);
#endif
}

int Stepper::increment(bool fwd) {
    if (fwd) {
        this->i = (this->i + 1) % this->N;
    } else {
        this->i = (this->i ? this->i : this->N) - 1;
    }

    return this->i % 4;
}

void Stepper::position(int pos, int mode) {
    // SOLO
    // -------
    // N | A B
    // 0 | < -
    // 1 | - <
    // 2 | > -
    // 3 | - >

    // DUAL
    // -------
    // N | A B
    // 0 | < <
    // 1 | > <
    // 2 | > >
    // 3 | < >
    
    // HALF
    // -------
    // N | A B
    // 0 | < -
    //   | < <
    // 1 | - <
    //   | > <
    // 2 | > -
    //   | > >
    // 3 | - >
    //   | < >
    
    switch (mode) {
    case SOLO:
        ain1_state = pos == 0;
        bin1_state = pos == 1;
        ain2_state = pos == 2;
        bin2_state = pos == 3;
        break;

    case DUAL:
        ain1_state = pos == 0 || pos == 1;
        bin1_state = pos == 1 || pos == 2;
        ain2_state = pos == 2 || pos == 3;
        bin2_state = pos == 3 || pos == 0;
        break;
    }
}

void Stepper::transition(int pos, int i) {
    if (pos == 0 || pos == 2) {
        apwm_state = sine_table[i];
        bpwm_state = sine_table[32-i];
    } else {
        apwm_state = sine_table[32-i];
        bpwm_state = sine_table[i];
    }
}

void Stepper::prepare() {
    digitalWrite(apwm, HIGH);
    digitalWrite(bpwm, HIGH);
    digitalWrite(stby, HIGH);
    apwm_state = 255;
    bpwm_state = 255;
}

void Stepper::stepSolo(bool fwd) {
    int n = this->increment(fwd);

    this->prepare();
    this->position(n, SOLO);
    this->set();
}

void Stepper::stepDual(bool fwd) {
    int n = this->increment(fwd);

    this->prepare();
    this->position(n, SOLO);
    this->set();
}

void Stepper::stepHalf(bool fwd, int delay) {
    int n = this->increment(fwd);

    this->prepare();
    this->position(n, DUAL);
    this->set();
    delayMicroseconds(delay);
    this->position(n, SOLO);
    this->set();
}

void Stepper::stepMicro(bool fwd, int micros, int delay) {
    int n = this->increment(fwd);

    if (micros > 5) {
        micros = 5;
    } else if (micros < 1) {
        micros = 1;
    }

    this->prepare();
    this->transition(n, 0);
    if (fwd) this->position(n, DUAL);
    this->set();

    int delayPerMicro = delay >> micros;

    int increment = 32 >> micros;
    for (int i = increment; i < 32; i += increment) {
        this->transition(n, i);
        this->set();
        delayMicroseconds(delayPerMicro);
    }

    this->transition(n, 32);
    this->position(n, SOLO);
    this->set();
    delayMicroseconds(delayPerMicro);
}